import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
from scipy.signal import triang

x = triang(63)
xbuf = np.zeros(128)

xbuf[:32] = x[31:]
xbuf[(128-31):] = x[:31]


XBUF = (fft(xbuf))
X = (fft(x))

plt.plot(x)
plt.plot(xbuf)

plt.figure(2)
plt.plot(abs(X))
plt.plot(abs(XBUF))

plt.figure(3)
plt.plot(np.unwrap(np.angle(X)))
plt.plot(np.unwrap(np.angle(XBUF)))
plt.show()
