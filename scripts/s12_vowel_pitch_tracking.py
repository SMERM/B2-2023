import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from s0_functions import *
from scipy.io.wavfile import write, read

# leggo un file audio in formato wav
[fs, sample]=read("../sounds/vocali_i.wav")

print("SR:", fs, "\ndimensione del file audio:", np.size(sample))

# normalizzo
sample = sample * 0.9 / np.max(abs(sample))

frame_size = 2048
# calcolo i margini della banda delle quefrenze
period_lb = 1 / 300.0
period_ub = 1 / 70
# il vettore che conterrà i pitch calcolati
pitches = []

STS = sts(sample, frame_size)
RMS = [rms(f) for f in STS]

# la matrice delle frame stft
f, t, STFT = signal.stft(sample, fs, window='blackman', nperseg=frame_size, noverlap=frame_size//2, nfft=frame_size, return_onesided=True, padded=True)
CS = np.zeros((STFT.shape[0]//2+1,STFT.shape[1]))
# ciclo sulle colonne della matrice stft
for fidx in range(STFT.shape[1]):
    time_vector = np.arange(frame_size) / fs
    dt = 1 / fs
    freq_vector = np.fft.rfftfreq(frame_size, d=dt)
    # calcolo il cepstrum a partire dalla frame della DFT
    CS[:,fidx] = np.abs(np.fft.rfft(np.log(np.abs(STFT[:,fidx]))))
    # calcolo il vettore delle quefrenze
    quefrencies = np.array(range(len(CS[:,fidx]))) / fs
    max_cep = 0
    max_que = 0
    # applico il "liftering" e seleziono il picco max nella banda delle quefrenze
    for i, quefrency in enumerate(quefrencies):
        if quefrency < period_lb or quefrency > period_ub:
            continue

        if CS[i,fidx] > max_cep:
            max_cep = CS[i,fidx]
            max_que = quefrency
    # salvo il valore del periodo trovato
    pitches.append(max_que)
CS[0,:] = 0
pitches = 1 / (2 * np.array(pitches)) # inverto per ottenere i valori in frequenza
plt.figure(1)
plt.subplot(211)
plt.plot(10*np.log(RMS))
plt.ylabel('RMS (dB)')
plt.xlabel('frame')
plt.subplot(212)
plt.plot(pitches)
plt.xlabel('frame')
plt.ylabel('pitch')

plt.figure(2)
plt.plot(np.abs(CS[:,28]))
plt.ylabel('RMS (dB)')
plt.xlabel('frame')


plt.show()