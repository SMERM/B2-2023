import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft, fftshift
from scipy.io.wavfile import read

[fs, x] = read("../sounds/oboe-A4.wav")
N = 512
M = 401
hN = N//2
hM = (M+1)//2
start = int(.8*fs)
xw = x[start-hM:start+hM-1] * np.hamming(M)

xwzp = np.zeros(N)
xwzp[:M] = xw

plt.figure(1, figsize=(9.5, 6.5))
plt.subplot(321)
plt.plot(np.arange(0, N), xwzp, lw=1.5)
plt.axis([0, N, min(xw), max(xw)])
plt.title('x (oboe-A4.wav), M = ' + str(M))

fftbuffer = np.zeros(N)
fftbuffer[:hM] = xw[hM-1:]
fftbuffer[N-hM+1:] = xw[:hM-1]

plt.subplot(322)
plt.plot(np.arange(0, N), fftbuffer, lw=1.5)
plt.axis([0, N, min(xw), max(xw)])
plt.title('fftbuffer: N = ' + str(N))


X = fftshift(fft(xwzp))
mX = 20 * np.log10(abs(X)/N)
pX = np.unwrap(np.angle(X))
plt.subplot(323)
plt.plot(np.arange(-hN, hN), mX, 'r', lw=1.5)
#plt.plot(np.arange(-hM, hM-1), mX, 'r', lw=1.5)
plt.axis([-hN,hN-1,-100,max(mX)])
#plt.axis([-hM,hM-1,-100,max(mX)])
plt.title('mX')

plt.subplot(325)
plt.plot(np.arange(-hN, hN), pX, 'c', lw=1.5)
#plt.plot(np.arange(-hM, hM-1), pX, 'c', lw=1.5)
plt.axis([-hN,hN-1,min(pX),max(pX)])
plt.title('pX')

XB = fftshift(fft(fftbuffer))
mXB = 20 * np.log10(abs(XB)/N)
pXB = np.unwrap(np.angle(XB))
plt.subplot(324)
plt.plot(np.arange(-hN, hN), mXB, 'r', lw=1.5)
plt.axis([-hN,hN-1,-100,max(mXB)])
plt.title('mXB')

plt.subplot(326)
plt.plot(np.arange(-hN, hN), pXB, 'c', lw=1.5)
plt.axis([-hN,hN-1,min(pXB),max(pXB)])
plt.title('pXB')

plt.tight_layout()
plt.savefig('fft-zero-phase.png')
plt.show()
