from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import write, read
from s0_functions import *

sample_freq = 44100
frame_size = 4096
time_vector = np.arange(frame_size) / sample_freq

signal = gen_harmonic_wave(200, sample_freq, frame_size, n_harmonics=50)

windowed_signal = np.hamming(frame_size) * signal

X = np.fft.rfft(windowed_signal)
log_X = np.log(np.abs(X))

freq_vector, H = gen_formant_filter(sample_freq,frame_size,2000,0.1)

Y = X * H
log_Y = np.log(np.abs(Y))
log_H = np.log(np.abs(H))

XC = np.fft.rfft(log_X)
YC = np.fft.rfft(log_Y)
HC = np.fft.rfft(log_H)
print("segnale: ", XC[0], "filtro: ", HC[0], "uscita: ",YC[0])
df = freq_vector[1] - freq_vector[0]
quefrency_vector = np.fft.rfftfreq(log_X.size, df)


fig, ax = plt.subplots()
ax.plot(time_vector, signal)
ax.set_xlabel('time (s)')
ax.set_title('time signal')


plt.figure(2)
plt.title('Fourier log spectrum')
plt.subplot(3,1,1)
plt.plot(freq_vector, log_X)
plt.subplot(3,1,2)
plt.plot(freq_vector, np.abs(H))
plt.subplot(3,1,3)
plt.plot(freq_vector, np.abs(Y))
plt.xlabel('frequency (Hz)')

plt.figure(3)
plt.subplot(3,1,1)
plt.plot(quefrency_vector, np.abs(XC))
plt.subplot(3,1,2)
plt.plot(quefrency_vector, np.abs(HC))
plt.subplot(3,1,3)
plt.plot(quefrency_vector, np.abs(YC))
plt.xlabel('quefrency (s)')

plt.show()