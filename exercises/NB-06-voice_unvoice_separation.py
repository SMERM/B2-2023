#
# spectral flatness
#
import pdb
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
from scipy.signal import stft
import math

##### MAIN

# leggo file audio

[fs, segnale] = read("../sounds/speech-female.wav")

# normalizziamo il segnale
max_segnale = np.max(np.abs(segnale))
segnale = segnale / max_segnale
dur = len(segnale)/fs

#
# da qui inizia l'esercizio 05 vero e proprio
#
inner_frame = 1001
overlap=inner_frame//4
f, t, STFT = stft(segnale, fs=fs, window='blackman', nperseg=inner_frame, noverlap=overlap, nfft=2048)

#
# calcolo della spectral flatness
#
# per evitare di uscire fuori dal dominio dei float, converto i valori
# assoluti della frame da lineari a logaritmici e poi li ri-espando
#
def spectral_flatness(frame):
    abs_frame = np.abs(frame)
    fsize = len(abs_frame)
    log_abs_frame = np.log(abs_frame)
    geometric_mean = np.exp(np.average(log_abs_frame))
    arith_mean = np.sum(abs_frame)/fsize
    return 20*np.log10(geometric_mean/arith_mean)

#
# la STFT ritorna i bins nelle colonne e i frames nelle righe, quindi devo
# ruotare la matrice di 90 gradi.
#
rot_STFT = np.transpose(STFT)
flatness = np.zeros(len(rot_STFT))
for n, frame in enumerate(rot_STFT):
    flatness[n] = spectral_flatness(frame)

flatness_thresh = -22 # deciso con verifica ottica del risultato
segnale_vocali = np.zeros(len(segnale))
segnale_conson = np.zeros(len(segnale))
win_fun = np.blackman(inner_frame)

for n, flat_value in enumerate(flatness):
    frame_start = n * overlap
    frame_end = frame_start + inner_frame
    #
    # flat_value < flatness_thresh == voiced spectrum
    # flat_value >= flatness_thresh == noisy spectrum
    #
    if flat_value >= flatness_thresh:
        segnale_conson[frame_start:frame_end] += (segnale[frame_start:frame_end]*win_fun)
    else:
        segnale_vocali[frame_start:frame_end] += (segnale[frame_start:frame_end]*win_fun)

write("./NB-06-vocali.wav", fs, segnale_vocali)
write("./NB-06-conson.wav", fs, segnale_conson)

plt.figure(1, figsize=(16,8))
plt.subplot(211)
plt.plot(segnale_vocali, 'b', label='vocali')
plt.legend()
plt.subplot(212)
plt.plot(segnale_conson, 'r', label='consonanti')
plt.legend()
plt.show()
