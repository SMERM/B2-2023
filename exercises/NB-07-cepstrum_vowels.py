#
# spectral flatness
#
import sys
sys.path.append('../scripts')
import pdb
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
from scipy.signal import stft
from s0_functions import *
import math

##### MAIN

# leggo file audio
frame_size = 4096

[fs, sig_a] = read("../sounds/vocali_a.wav")
[fs, sig_i] = read("../sounds/vocali_i.wav")

# normalizziamo i segnali
max_sig_a = np.max(np.abs(sig_a))
sig_a = sig_a / max_sig_a
dur_a = len(sig_a)/fs
# prendo 4096 campioni in mezzo
half_a = int(dur_a/2)
sig_a = sig_a[half_a:half_a + frame_size]

max_sig_i = np.max(np.abs(sig_i))
sig_i = sig_i / max_sig_i
dur_i = len(sig_i)/fs
half_i = int(dur_i/2)
sig_i = sig_i[half_i:half_i + frame_size]

#
# da qui inizia l'esercizio 05 vero e proprio
#
windowed_sig_a = np.hamming(frame_size) * sig_a
windowed_sig_i = np.hamming(frame_size) * sig_i

X_a = np.fft.rfft(windowed_sig_a)
log_X_a = np.log(np.abs(X_a))
X_i = np.fft.rfft(windowed_sig_i)
log_X_i = np.log(np.abs(X_i))
freq_vector = np.fft.rfftfreq(X_a.size, d=(1/fs))

df = freq_vector[1] - freq_vector[0]
quefrency_vector = np.fft.rfftfreq(log_X_a.size, df)

XC_a = np.fft.rfft(log_X_a)
XC_i = np.fft.rfft(log_X_i)
XC_a[0] = 0.0
XC_i[0] = 0.0

exc_freq = np.array([70, 300])
exc_per  = 1/exc_freq

plt.subplot(2,1,1)
plt.plot(quefrency_vector, np.abs(XC_a))
plt.axis([exc_per[1], exc_per[0], 0, 100])
plt.xlabel('quefrency (s)')
plt.subplot(2,1,2)
plt.plot(quefrency_vector, np.abs(XC_i))
plt.axis([exc_per[1], exc_per[0], 0, 100])
plt.xlabel('quefrency (s)')

plt.show()
