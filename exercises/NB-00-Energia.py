#
# cannibalizzato dallo script ../scripts/6-short time signal analysis.py
#
import pdb
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
import math

# CREAZIONE DELLA MATRICE DELLE FRAME TEMPORALI

def sts (sig, frameSize):
    # calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig)/frameSize)

    STS = np.zeros((Nframe,frameSize))

    # copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe*frameSize)

    sigFramed[:len(sig)] = sig

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):((i+1)*frameSize)]

    return [STS, Nframe]

# RMS

def rms(v):
    v = np.square(v)
    v = np.mean(v)
    v = np.sqrt(v)
    return v

##### MAIN

# leggo file audio

[fs, segnale] = read("../sounds/speech-male.wav")

# normalizziamo il segnale
segnale = segnale / np.max(abs(segnale))

#
# da qui inizia l'esercizio 00 vero e proprio
#
inner_frame = 501
outer_frame = 512
(STS, nframes) = sts(segnale,inner_frame)       # finestra dispari per zero-phase windowing
RMS = [rms(f) for f in STS]

#
# STFT sul segnale
#
STFT = np.zeros((nframes, outer_frame))
idx = 0
for frame in STS:
    fsize = len(frame)
    #
    # zero-padding
    #
    zpside = int(np.floor((outer_frame-inner_frame)/2))
    outer = np.zeros((1, outer_frame))
    outer[0][zpside:zpside+fsize] = frame
    #
    # zero-phase windowing
    #
    zpwin = np.zeros((1, outer_frame))
    half_win = int(outer_frame/2)
    zpwin[0][0:half_win] = outer[0][half_win:]
    zpwin[0][half_win+1:] = outer[0][0:half_win-1]
    #
    # FFT modulo re-normalized for real signals
    #
    tempfft = np.abs(fft(zpwin))/(outer_frame*2)
    STFT[idx] = tempfft
    idx += 1

#
# calcolo dell'energia dello spettro (solo parte reale)
#
spect_en = np.zeros((1, nframes))
idx = 0
for frame in STFT:
    spect_en[0][idx] = np.sum(frame[0:int(outer_frame/2)])
    idx += 1


plt.plot(spect_en[0], 'b', label='spectrum')
plt.plot(RMS, 'r', label='RMS')
plt.legend()
plt.show()
