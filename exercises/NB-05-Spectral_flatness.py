#
# spectral flatness
#
import pdb
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
from scipy.signal import stft
import math

##### MAIN

# leggo file audio

[fs, segnale] = read("../sounds/speech-male.wav")
sinc = 1.0 / fs

# normalizziamo il segnale
max_segnale = np.max(np.abs(segnale))
segnale = segnale / max_segnale
dur = len(segnale)/fs
tlin = np.linspace(0, dur, len(segnale))

#
# da qui inizia l'esercizio 05 vero e proprio
#
inner_frame = 1001
f, t, STFT = stft(segnale, fs=fs, nperseg=inner_frame, noverlap=inner_frame//4, nfft=2048)

# plt.figure(2, figsize=(16, 8))
# plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=0.05, shading='gouraud')
# plt.axis([0, len(segnale)/fs, 0, 5000])
# plt.title('STFT Magnitude')
# plt.ylabel('Frequency [Hz]')
# plt.xlabel('Time [sec]')
# plt.show()

#
# calcolo della spectral flatness
#
# per evitare di uscire fuori dal dominio dei float, converto i valori
# assoluti della frame da lineari a logaritmici e poi li ri-espando
#
def spectral_flatness(frame, n):
    abs_frame = np.abs(frame)
    fsize = len(abs_frame)
    log_abs_frame = np.log(abs_frame)
    geometric_mean = np.exp(np.average(log_abs_frame))
    arith_mean = np.sum(abs_frame)/fsize
    return 20*np.log10(geometric_mean/arith_mean) # moltiplicato 20 perché trattiamo intensità lineari

#
# la STFT ritorna i bins nelle colonne e i frames nelle righe, quindi devo
# ruotare la matrice di 90 gradi.
#
rot_STFT = np.transpose(STFT)
flatness = np.zeros(len(t))
for n, frame in enumerate(rot_STFT):
    flatness[n] = spectral_flatness(frame, n)

plt.figure(1, figsize=(16,8))
plt.subplot(211)
plt.grid(True)
plt.plot(tlin, segnale, 'r', label='segnale')
plt.ylabel('Amp')
plt.xlabel('Time [sec]')
plt.subplot(212)
plt.grid(True)
plt.plot(t, flatness, 'b', label='flatness')
# plt.axis([0, t[-1], np.min(flatness)*0.75, np.max(flatness)*1.25])
plt.ylabel('Flatness [dB]')
plt.xlabel('Time [sec]')
plt.legend()
plt.show()
